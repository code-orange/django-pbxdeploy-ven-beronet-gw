import os

from django.core.management.base import BaseCommand
from django.template import engines
from lxml import etree

from django_pbxdeploy_ven_beronet_gw.models import PsAuths


class Command(BaseCommand):
    help = "Create new transaction"

    def add_arguments(self, parser):
        parser.add_argument("beromodel", type=str)
        parser.add_argument("trunkid", type=str)
        parser.add_argument("local_prefix", type=int)
        parser.add_argument("main_num", type=int)

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        django_engine = engines["django"]

        beromodel = options["beromodel"]
        trunkid = options["trunkid"]
        local_prefix = options["local_prefix"]
        main_num = options["main_num"]

        # find trunk
        trunk = PsAuths.objects.using("mediasw_conf").get(id=trunkid)

        template_opts = dict()
        template_opts["sip_auth_user"] = trunk.username
        template_opts["sip_auth_passwd"] = trunk.password
        template_opts["local_prefix"] = str(local_prefix)
        template_opts["main_num"] = str(main_num)

        xml_config_file = open("tmp/beroNetVoIPGateway.config.xml", "wb")

        xml_config = etree.Element("beroNetProvisioning")
        config = etree.Element("Config")

        update = etree.Element("Update")
        update.text = "yes"
        config.append(update)

        activate = etree.Element("Activate")
        activate.text = "3"
        config.append(activate)

        for root, dirs, files in os.walk(
            "django_pbxdeploy_ven_beronet_gw/templates/django_pbxdeploy_ven_beronet_gw/general"
        ):
            for filename in files:
                config_template_file = open(os.path.join(root, filename), "r").read()
                config_template = django_engine.from_string(config_template_file)

                file = etree.Element("File")

                file_name = etree.Element("Name")
                file_name.text = filename
                file.append(file_name)

                file_contents = etree.Element("Contents")
                file_contents.text = config_template.render(template_opts)
                file.append(file_contents)

                config.append(file)

        for root, dirs, files in os.walk(
            "django_pbxdeploy_ven_beronet_gw/templates/django_pbxdeploy_ven_beronet_gw/model-specific/"
            + beromodel
        ):
            for filename in files:
                config_template_file = open(os.path.join(root, filename), "r").read()
                config_template = django_engine.from_string(config_template_file)

                file = etree.Element("File")

                file_name = etree.Element("Name")
                file_name.text = filename
                file.append(file_name)

                file_contents = etree.Element("Contents")
                file_contents.text = config_template.render(template_opts)
                file.append(file_contents)

                config.append(file)

        xml_config.append(config)

        xml_config_str = etree.tostring(
            xml_config, pretty_print=True, xml_declaration=True, encoding="UTF-8"
        )
        xml_config_file.write(xml_config_str)
        xml_config_file.close()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
